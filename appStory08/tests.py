from django.test import TestCase, Client
import unittest
from django.urls import resolve
from .views import *

# Create your tests here.
class Story08_Routing_URL_unit_test(TestCase):
    def test_story8_url_is_exist(self):
        response = Client().get('/')
        self.assertEqual(response.status_code, 200)

    def test_Story08_using_index_func(self):
        found = resolve('/')
        self.assertEquals(found.func, index)
    
    def test_Story08_using_story08_html(self):
        response = Client().post('/')
        self.assertTemplateUsed(response, 'story08.html')

class Story08_Search_a_book_unit_test(TestCase):
    def test_find_a_book_is_exist(self):
        response = Client().get('/')
        content = response.content.decode('utf8')
        self.assertIn('Find a Book', content)

    def test_text_box_is_exist(self):
        response = Client().get('/')
        content = response.content.decode('utf8')
        self.assertIn('id="search"', content)

    def test_button_is_exist(self):
        response = Client().get('/')
        content = response.content.decode('utf8')
        self.assertIn('id="button"', content)

    
    
