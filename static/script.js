function ajaxCall(value) {
    $('tbody').empty()
    let key = value
    $.ajax({
        method: 'GET',
        url: 'https://www.googleapis.com/books/v1/volumes?q=' + key,
        success: function(response) {
            for (let i = 0; i < response.items.length; i++) {
                let Body = document.createElement('tr')
                let No = document.createElement('td')
                No.innerText = i + 1
				Body.append(No)
				
				let Img = document.createElement('td')
                if (response.items[i].volumeInfo.imageLinks !== undefined) {
                    let image = document.createElement('img')
                    image.src = response.items[i].volumeInfo.imageLinks.thumbnail
                    Img.append(image)
                } else {
                    let image = document.createElement('img')
                    image.alt = "No Image Available"
                    Img.append(image)
                }

                let Title = document.createElement('td')
                Title.innerText = response.items[i].volumeInfo.title

                let Author = document.createElement('td')
                if (response.items[i].volumeInfo.authors !== undefined) {
                    for (let j = 0; j < response.items[i].volumeInfo.authors.length; j++) {
                        Author.innerText = response.items[i].volumeInfo.authors[j]
                    }
                } else {
                    Author.innerText = 'Author(s) Unknown'
                }
                
                Body.append(Img,Title, Author)
                $('tbody').append(Body)            
            }
        },
        error: function() {
            console.log('Failed')
        }
    })
}
$(document).ready(function() {
	$('#button').on('click', function() {
		$('.table-content').show()
		let key = $('#search').val()
		ajaxCall(key)
	})

})

 
